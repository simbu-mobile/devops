# https://docs.codemagic.io/yaml/yaml-getting-started/
# https://docs.codemagic.io/yaml-quick-start/codemagic-sample-projects/

reusable:
  email_recipients: &email_recipients
    simbu@outlook.com
  flutter_version: &flutter_version stable
  scripts:
    - &pub_get
      name: Get dependencies
      script: |
        flutter clean
        flutter packages pub get

    - &run_code_metrics
      name: Analyse and gather code metrics
      script: |
        flutter pub global activate dart_code_metrics
        mkdir -p metrics-results
        flutter pub global run dart_code_metrics:metrics analyze lib --reporter=json > metrics-results/dart_code_metrics.json      
      test_report: metrics-results/dart_code_metrics.json

    - &run_local_unit_and_widget_tests
      name: Unit and Widget tests
      script: |
        mkdir -p test-results 
        flutter test --machine > test-results/local-test-results.json      
      test_report: test-results/local-test-results.json

    - &ios_setup_keychain
      name: iOS - Set up keychain for code signing
      script: keychain initialize

    - &ios_fetch_signing_files
      name: iOS - Fetch signing files
      script: app-store-connect fetch-signing-files "com.7im.demo" --type IOS_APP_ADHOC --create
    
    - &ios_setup_signing_cert      
      name: iOS - Set up signing certificate
      script: keychain add-certificates

    - &ios_set_project_codesign_settings
      name: iOS - Set signing settings on Xcode project
      script: xcode-project use-profiles
    
    - &ios_build
      name: iOS - Build .ipa
      script: |      
        find . -name "Podfile" -execdir pod install \;
        flutter build ipa --export-options-plist=/Users/builder/export_options.plist

    - &android_debug_keystore
      name: Android - Set up debug key.properties
      script: |
        keytool -genkeypair \
          -alias androiddebugkey \
          -keypass android \
          -keystore ~/.android/debug.keystore \
          -storepass android \
          -dname 'CN=Android Debug,O=Android,C=US' \
          -keyalg 'RSA' \
          -keysize 2048 \
          -validity 10000    
    
    - &android_setup_local_props
      name: Android - Set up local.properties
      script: echo "flutter.sdk=$HOME/programs/flutter" > "$FCI_BUILD_DIR/android/local.properties"

    - &android_build_apk 
      name: Android - Build .apk  
      script: flutter build appbundle --debug #flutter build apk --debug

    - &android_build_test_apk 
      name: Android - Build test .apk for TestLabs  
      script: |
        set -ex
        cd android
        ./gradlew app:assembleAndroidTest
        ./gradlew app:assembleDebug -Ptarget="$FCI_BUILD_DIR/integration_test/app_test.dart" 

    - &firebase_run_testlab_tests
      name: Firebase - Run TestLab tests
      script: |
        set -ex
        echo $GCLOUD_KEY_FILE > ./gcloud_key_file.json
        gcloud auth activate-service-account --key-file=gcloud_key_file.json
        gcloud --quiet config set project $FIREBASE_PROJECT
        gcloud firebase test android run \
          --type instrumentation \
          --app build/app/outputs/apk/debug/app-debug.apk \
          --test build/app/outputs/apk/androidTest/debug/app-debug-androidTest.apk \
          --device model=dreamlte,orientation=portrait  \
          --timeout 3m

workflows:
  integrate-workflow:   # CI - Continuous Integration      
    name: Integration                 
    instance_type: mac_mini  
    max_build_duration: 20   
    environment:
      flutter: *flutter_version
      xcode: latest
      cocoapods: default
      groups:
        - appstore_credentials
        - firebase_credentials
    scripts:
      - *ios_setup_keychain
      - *ios_fetch_signing_files
      - *ios_setup_signing_cert 
      - *ios_set_project_codesign_settings
      - *android_debug_keystore
      - *android_setup_local_props
      - *pub_get
      - *android_build_apk
      - *run_code_metrics
      - *run_local_unit_and_widget_tests
      - *android_build_test_apk
      - *firebase_run_testlab_tests
      - *ios_build
    artifacts:
      - metrics-results/dart_code_metrics.json
      - test-results/local-test-results.json
      - build/ios/ipa/*.ipa
      - build/**/outputs/apk/**/*.apk
      - build/**/outputs/bundle/**/*.aab
      - build/**/outputs/**/mapping.txt
      - flutter_drive.log
    publishing:
      email:
        recipients:
          - *email_recipients
        notify:
          success: true 
          failure: false
